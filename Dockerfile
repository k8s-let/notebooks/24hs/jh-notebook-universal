FROM registry.ethz.ch/k8s-let/notebooks/24hs/jh-notebook-base:4.1.1-01

USER root

#######
# Julia
#######

RUN http_proxy=http://proxy.ethz.ch:3128 https_proxy=http://proxy.ethz.ch:3128 julia -e "using Pkg; Pkg.add.([ \
	\"NBInclude\", \ 
	\"PyPlot\", \ 
	\"Roots\", \
	\"UnPack\", \ 
        \"WhereTheWaterFlows\", \
        \"BenchmarkTools\", \
	\"Plots\" \
]); Pkg.update;"

####
# R
####
RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c( \
    'bayesplot', \
    'bayestestR', \
    'blockmodeling', \
    'biostat3', \
    'brms', \
    'countrycode', \
    'GGally', \
    'ggforce', \
    'ggraph', \
    'goldfish', \
    'gt', \
    'gtsummary', \
    'igraph', \
    'lfe', \
    'loo', \
    'matlib', \
    'mice', \
    'nloptr', \ 
    'RBGL', \
    'RColorBrewer', \
    'RcppEigen', \
    'RSiena', \
    'sbm', \
    'scico', \
    'sensitivity', \
    'sjPlot', \
    'sna', \
    'stargazer', \
    'statnet', \
    'tidybayes', \
    'tidygraph', \
    'tidyverse'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)" \
    && \
    Rscript -e "install.packages(pkgs=c('BiocManager'), repos=c('http://cran.r-project.org'))" \
    && \
    Rscript -e "BiocManager::install()" \
    && \
    Rscript -e "devtools::install_github(repo = 'AECP-ETHZ/ETH.OLP')"

##############################
# Python and R from Condaforge
##############################

RUN mamba install -q -c defaults -c conda-forge -c bioconda -c bokeh \
  arviz \
  biopython \
  bokeh \
  boost-histogram \
  corner \
  cobra \
  dynesty \
  emcee \
  ete3 \
  flax \
  fqdn \
  geopandas \
  graph-tool \
  igraph \
  isoduration \
  jupyterlab-vpython \
  keras \
  meshio \
  mplcursors \
  multi_rake \
  nbgitpuller \
  optax \
  pathos \
  pint \
  plotly \
  py3dmol \
  pylatex \
  pyteomics \
  pytesseract \
  pyspellchecker \
  rdkit \
  root \
  scikit-image \
  scikit-learn \
  seaborn \
  sparqlwrapper \
  tabulate \
  tinygp \
  trame \
  trame-vuetify \
  uncertainties \
  uri-template \
  webcolors \
  wikipedia \
  wordcloud \
  yake \
  r-ape \
  r-bio3d \
  r-devtools \
  r-lpsolve \
  r-lhs \
  r-rsolnp \
  r-terra \
  && \
  mamba install -c python-control control \
  && \
  mamba clean --all

# Disabled:
# autogluon \


##################
# Python from pypi
##################

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  anndata \
  asserts \
  astroquery \
  brainpy \
  Cartopy \
  deep_translator \
  einsteinpy \
  gmsh \
  goatools \
  gurobipy \
  jaxopt \
  jupyter_ai \
  lmfit \
  mycolorpy \
  mpl_point_clicker \
  networkit \
  networkx \
  numdifftools \
  jupyterlab-deck \
  keybert \
  obspy \
  openai \
  opencv-python opencv-contrib-python \
  otter-grader \
  petitRADTRANS \
  probfit \
  PyAstronomy \
  pycosmo \
  pycryptodome \
  pyFAI \
  pymatgen \
  pyvista \
  "tensorflow-probability[jax]" \
  torch \
  rise \
  sensemapi \
  scanpy \
  scikit-image \
  scikit-kinematics \
  summa \
  umap \
  umap-learn \
  vpython \
  wiktionaryparser \
  yolov5

# =====================
# Packages requested after semester start, should go to the list 2 RUNs above if a major rebuild is planned.
# Placing it here keeps the image cache disk space low, because it adds only a thin extra layer
# =====================

# No extra package added yet for FS24...
# RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install -U --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
#  package_1 \
#  package_last

# User submitted support programs
COPY bin /usr/local/bin/

# Configuration items
COPY etc /etc

USER 1000
